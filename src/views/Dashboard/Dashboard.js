import React, { Component } from "react";
import TopicContainer from "../../components/TopicContainer";
import { inject, observer } from "mobx-react";

class Dashboard extends Component {
  handleClick = () => {
    const { history } = this.props;
    history.push("/new-topic");
  };

  render() {
    const { topicStore } = this.props;
    const { topicContentArray } = topicStore;
    // sort topic array based on most up vote
    const sortedContent = topicContentArray.sort((valuePrev, valueNext) => {
      return valueNext.up - valuePrev.up;
    });

    return (
      <div className="animated fadeIn">
        <button onClick={this.handleClick} className="custom-btn">
          New Topic
        </button>
        {/* validate sorted content variable is not an empty array */}
        {sortedContent.length > 0 ? (
          sortedContent.map((content, index) => {
            if (index < 20) {
              return <TopicContainer content={content} />;
            }
          })
        ) : (
          <h1>Please Add New Topic</h1>
        )}
      </div>
    );
  }
}

export default inject("topicStore")(observer(Dashboard));
