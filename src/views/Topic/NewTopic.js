import React, { Component } from "react";
import { observer, inject } from "mobx-react";
import { TextField } from "@material-ui/core";

class NewTopic extends Component {
  handleChange = event => {
    const { topicStore } = this.props;
    const { value, name } = event.target;
    // set new object to edited data with name from text field name, and the value from user input
    topicStore.setEditedData({
      [name]: value
    });
  };

  handleBack = () => {
    const { history } = this.props;
    history.push("/");
  };

  handleAdd = async () => {
    const { topicStore, history } = this.props;
    const { editedData } = topicStore;
    // check current topic id from local storage
    // initialize topic id value to 0 if there is no current topic id on local storage
    let topicId = localStorage.getItem("currentTopicId")
      ? parseFloat(localStorage.getItem("currentTopicId"))
      : 0;

    // push new topic object to array variable in store with default upvote an downvote is 0
    await topicStore.topicContentArray.push({
      id: parseFloat(topicId) + 1,
      topic: editedData.topic,
      up: 0,
      down: 0
    });

    // add topic id by 1 after add new topic
    topicId = parseFloat(topicId) + 1;

    // then set topic id to local storage
    localStorage.setItem("currentTopicId", topicId);

    // also set topic array to local storage
    localStorage.setItem(
      "topicArray",
      JSON.stringify(topicStore.topicContentArray)
    );

    // clear input field after add new topic
    topicStore.setEditedData({
      topic: ""
    });

    // redirect to dashboard
    history.push("/");
  };

  render() {
    const { topicStore } = this.props;
    const { editedData, joiFormErrors } = topicStore;
    return (
      <div className="animated fadeIn">
        <button onClick={this.handleBack} className="custom-btn">
          Back
        </button>
        <h1>Add New Topic</h1>
        <TextField
          label="Topic"
          placeholder="Input new topic here"
          name="topic"
          value={editedData.topic}
          onChange={this.handleChange}
          margin="normal"
          variant="outlined"
          className="w-100"
        />

        {/* check error message from joi */}
        {joiFormErrors && joiFormErrors["topic"] && (
          <span className="warning-text">{joiFormErrors["topic"]}</span>
        )}
        <br />
        <br />
        <button
          onClick={this.handleAdd}
          disabled={!!joiFormErrors}
          className="custom-btn"
        >
          Add
        </button>
      </div>
    );
  }
}

export default inject("topicStore")(observer(NewTopic));
