const Joi = require("joi-browser");
const joiToForms = require("joi-errors-for-forms").form;
const convertToForms = joiToForms();

export const validateEditedData = ({ editedData, joiSchema }) => {
  // validate edited data based on schema that i create in store
  const joiResult = Joi.validate(editedData, joiSchema, {
    convert: true,
    abortEarly: false,
    stripUnknown: true
  });
  if (!joiResult.error) {
    return null;
  }
  return convertToForms(joiResult.error);
};
