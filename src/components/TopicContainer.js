import React, { Component } from "react";
import { observer, inject } from "mobx-react";

class TopicContainer extends Component {
  handleUpvote = id => {
    const { topicStore } = this.props;
    const { topicContentArray } = topicStore;

    // looping to find id of topic
    topicContentArray.map(content => {
      if (id === content.id) {
        // get up vote count from array
        let upCount = content.up ? content.up : 0;
        content.up = parseFloat(upCount) + 1;
      }
    });
    // update topic array on local storage with new up vote count
    localStorage.setItem("topicArray", JSON.stringify(topicContentArray));
  };

  handleDownvote = id => {
    const { topicStore } = this.props;
    const { topicContentArray } = topicStore;

    // ooping to find id of topic
    topicContentArray.map(content => {
      if (id === content.id) {
        // get down vote count from array
        let downCount = content.down ? content.down : 0;
        content.down = parseFloat(downCount) + 1;
      }
    });
    // update topic array on local storage with new down vote count
    localStorage.setItem("topicArray", JSON.stringify(topicContentArray));
  };

  render() {
    const { content } = this.props;
    return (
      <div id={content.id} className="topic-container">
        <span className="topic-content-container">{content.topic}</span>
        <div className="vote-container">
          <div className="mr-10">
            <span className="mr-5">{content.up}</span>
            <i
              className="cui-thumb-up"
              onClick={() => {
                this.handleUpvote(content.id);
              }}
            />
          </div>
          <div>
            <span className="mr-5">{content.down}</span>
            <i
              className="cui-thumb-down"
              onClick={() => {
                this.handleDownvote(content.id);
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default inject("topicStore")(observer(TopicContainer));
