import { action, decorate, observable, computed } from "mobx";
import { validateEditedData } from "../helper/functions";

const Joi = require("joi-browser");

class TopicStore {
  editedData = {};

  // check topic array on local storage
  // initialize topic content array variable to empty array if there is no topic arry on local storage
  topicContentArray = localStorage.getItem("topicArray")
    ? JSON.parse(localStorage.getItem("topicArray"))
    : [];

  // set input field value to edited data variable
  setEditedData = updatedData => {
    // keep old value of edited data
    this.editedData = {
      ...this.editedData,
      ...updatedData
    };
  };

  // COMPUTED
  get joiFormErrors() {
    if (this.editedData) {
      return validateEditedData({
        // create joi schema to validate edited data
        editedData: this.editedData,
        joiSchema: {
          topic: Joi.string()
            .max(255)
            .required()
        }
      });
    }

    return { error: true };
  }
}

decorate(TopicStore, {
  editedData: observable,
  topicContentArray: observable,

  joiFormErrors: computed,

  setEditedData: action
});

export default new TopicStore();
